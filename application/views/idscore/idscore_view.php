<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url('assets/css/style.css'); ?>">
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>" type="text/javascript"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script> -->
    <title>IdScore Search</title>
</head>

<body>
    <?php
    // print_r($result);
    // var_dump($result);

    $data=json_decode($result, true);
    // echo "<pre>";
    // print_r($data);
    $ktp = $data['data']['s:Envelope']['s:Body']['SmartSearchIndividualResponse']['SmartSearchIndividualResult']['a:Parameters']['a:IdNumbers']['a:IdNumberPairIndividual']['a:IdNumber'];
    // echo ($ktp);
    ?>

    <div class="container" style="margin-top: 20px;">
        <nav class="navbar navbar-dark bg-dark">
            <a class="navbar-brand" href="#">
                <img src="<?php echo base_url('assets/image/idscore.jpeg') ?>" width="30" height="80" class="d-inline-block align-top" alt="">
            </a>
        </nav>
    </div>

    <div class="container" style="margin-top: 20px;">
        <div class="card">
            <div class="col-sm-12" style="margin-top: 20px;">
                <h5 class="card-title">Individual</h5>
                <form method="POST" name="myForm">
                    <div class="row">
                        <div class="p-2 ml-2">
                            <i class="fas fa-search"></i>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <input type="text" name="ktp" id="ktp" class="form-control" placeholder="KTP">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <input type="text" name="dateOfBirth" id="dateOfBirth" class="form-control" placeholder="Date of Birth">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <div>
                                    <button type="button" name="search" value="search" id="search" class="btn btn-primary" onclick="searchForm()">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <br></br>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="table-responsive">
                        <table class="table" id="tableData">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">IDSCORE ID</th>
                                    <th scope="col">NATIONAL ID</th>
                                    <th scope="col">NAME</th>
                                    <th scope="col">DATE OF BIRTH</th>
                                    <th scope="col">ADDRESS</th>
                                </tr>
                            </thead>
                            <tbody class="customtable">
                                <?php
                                // for ($i=0;$i<count($data);$i++){
                                    // $row=$data[$i];  
                                    // print_r($row);
                                    ?>
                                        <tr>
                                            <td><?php echo $data['data']['s:Envelope']['s:Body']['SmartSearchIndividualResponse']['SmartSearchIndividualResult']['a:IndividualRecords']['a:SearchIndividualRecord']['a:PefindoId']?></td>
                                            <td><?php echo $data['data']['s:Envelope']['s:Body']['SmartSearchIndividualResponse']['SmartSearchIndividualResult']['a:IndividualRecords']['a:SearchIndividualRecord']['a:KTP']?></td>
                                            <td><?php echo $data['data']['s:Envelope']['s:Body']['SmartSearchIndividualResponse']['SmartSearchIndividualResult']['a:IndividualRecords']['a:SearchIndividualRecord']['a:FullName']?></td>
                                            <td><?php echo $data['data']['s:Envelope']['s:Body']['SmartSearchIndividualResponse']['SmartSearchIndividualResult']['a:IndividualRecords']['a:SearchIndividualRecord']['a:DateOfBirth']?></td>
                                            <td><?php echo $data['data']['s:Envelope']['s:Body']['SmartSearchIndividualResponse']['SmartSearchIndividualResult']['a:IndividualRecords']['a:SearchIndividualRecord']['a:Address']?></td>
                                        </tr>

                                    <?php
                                // }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function searchForm() {
            alert("Sending Json");
            let ktp = document.querySelector('#ktp');
            let dateOfBirth = document.querySelector('#dateOfBirth');
            let name = document.querySelector('#name');

            // Creating a XHR object
            let xhr = new XMLHttpRequest();
            let url = "http://192.168.64.2/ci3/index.php/smartSearchIndividual1";
       
            // open a connection
            xhr.open("POST", url, true);
 
            // Set the request header i.e. which type of content you are sending
            xhr.setRequestHeader("Content-Type", "application/json");
 
            // Create a state change callback
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {

                    var json = xhr.responseText;
                    var jsonResponse = JSON.parse(json);
                    console.log(jsonResponse["Data"])
                }
            };
 
            // Converting JSON data to string
            var data = JSON.stringify({ "dateOfBirth": dateOfBirth.value, "idNumberType": ktp.value, "fullName": name.value });
 
            // Sending data with the request
            xhr.send(data);
            console.log(data);
            // $.ajax({
            //     url:"http://192.168.64.2/ci3/index.php/smartSearchIndividual1",
            //     type:"POST",
            //     data:JSON.stringify({ "dateOfBirth": dateOfBirth.value, "idNumberType": ktp.value, "fullName": name.value }),
            //     contentType:"application/json; charset=utf-8",
            //     dataType:"json",
            //     success: function(data){
            //         alert(data);
            //     }
            // })
        }
    </script>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

    <script src="https://kit.fontawesome.com/801818056c.js" crossorigin="anonymous"></script>
</body>

</html>