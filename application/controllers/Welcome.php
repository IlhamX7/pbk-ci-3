<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		echo("Welcome to IdScore");
	}

	public function search()
	{
		print_r($_POST);
		die;
		$ktp = $_POST['ktp'];
		$ttl = $_POST['ttl'];
		$nama = $_POST['nama'];
		$res = [
			"status" => "success",
			"data" => [
				"ktp"=>$ktp,
				"nama"=>$nama,
				"ttl"=>$ttl
			]
			];
		echo json_encode($res);
	}

	public function smartSearchIndividual1(){

		$client = new \GuzzleHttp\Client([
			'base_uri' => 'http://192.168.1.5:3000',
			'defaults' => [
				'exceptions' => false
			]
		]);
		
		$response = $client->post('/smartSearchIndividual1', [
			GuzzleHttp\RequestOptions::JSON => [
				'dateOfBirth' => '1985-01-05',
				'idNumberType' => 3114050501850036,
				'fullName' => 'Andy firmansah',
			],
			GuzzleHttp\RequestOptions::HEADERS => ['Content-Type' => 'application/json',  'access-token' => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjQyNDgzMDQzfQ.glTwU6Lil0VPfVDL_uhR2JyZqif2_X6oVJr0Sr5xj70"]
		]);

		// echo $response->getBody()->getContents();
		$result = ($response->getBody()->getContents());
		$this->load->helper('url');
		$this->load->view('idscore/idscore_view',["result"=>$result]);

		}

	public function smartSearchIndividual2(){

		$client = new \GuzzleHttp\Client([
			'base_uri' => 'http://192.168.1.5:3000',
			'defaults' => [
				'exceptions' => false
			]
		]);
		
		$response = $client->post('/smartSearchIndividual2', [
			GuzzleHttp\RequestOptions::JSON => [
				'dateOfBirth' => '1982-10-05',
				'idNumber' => 123212312321,
				'fullName' => 'zetha',
			],
			GuzzleHttp\RequestOptions::HEADERS => ['Content-Type' => 'application/json',  'access-token' => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjQyNDgzMDQzfQ.glTwU6Lil0VPfVDL_uhR2JyZqif2_X6oVJr0Sr5xj70"]
		]);

		echo $response->getBody()->getContents();

		}

	public function smartSearchIndividual3(){

		$client = new \GuzzleHttp\Client([
			'base_uri' => 'http://192.168.1.7:3000',
			'defaults' => [
				'exceptions' => false
			]
		]);
		
		$response = $client->post('/smartSearchIndividual3', [
			GuzzleHttp\RequestOptions::JSON => [
				'dateOfBirth' => '1982-11-05',
				'idNumber' => 31241,
				'fullName' => 'Andara',
			],
			GuzzleHttp\RequestOptions::HEADERS => ['Content-Type' => 'application/json',  'access-token' => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjQyNDgzMDQzfQ.glTwU6Lil0VPfVDL_uhR2JyZqif2_X6oVJr0Sr5xj70"]
		]);

		echo $response->getBody()->getContents();

		}

	public function smartSearchCompany1(){

		$client = new \GuzzleHttp\Client([
			'base_uri' => 'http://192.168.1.7:3000',
			'defaults' => [
				'exceptions' => false
			]
		]);
		
		$response = $client->post('/smartSearchCompany1', [
			GuzzleHttp\RequestOptions::JSON => [
				'companyName' => 'PT Pertambangan Indonesia',
				'idNumber' => '032006752814012'
			],
			GuzzleHttp\RequestOptions::HEADERS => ['Content-Type' => 'application/json',  'access-token' => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjQyNDgzMDQzfQ.glTwU6Lil0VPfVDL_uhR2JyZqif2_X6oVJr0Sr5xj70"]
		]);

		echo $response->getBody()->getContents();

		}

	public function smartSearchCompany2(){

		$client = new \GuzzleHttp\Client([
			'base_uri' => 'http://192.168.1.7:3000',
			'defaults' => [
				'exceptions' => false
			]
		]);
		
		$response = $client->post('/smartSearchCompany2', [
			GuzzleHttp\RequestOptions::JSON => [
				'companyName' => 'Lumbung',
				'idNumber' => 2131241
			],
			GuzzleHttp\RequestOptions::HEADERS => ['Content-Type' => 'application/json',  'access-token' => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjQyNDgzMDQzfQ.glTwU6Lil0VPfVDL_uhR2JyZqif2_X6oVJr0Sr5xj70"]
		]);

		echo $response->getBody()->getContents();

		}

	public function smartSearchCompany3(){

		$client = new \GuzzleHttp\Client([
			'base_uri' => 'http://192.168.1.7:3000',
			'defaults' => [
				'exceptions' => false
			]
		]);
		
		$response = $client->post('/smartSearchCompany3', [
			GuzzleHttp\RequestOptions::JSON => [
				'companyName' => 'Griya Anda',
				'idNumber' => 123456
			],
			GuzzleHttp\RequestOptions::HEADERS => ['Content-Type' => 'application/json',  'access-token' => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjQyNDgzMDQzfQ.glTwU6Lil0VPfVDL_uhR2JyZqif2_X6oVJr0Sr5xj70"]
		]);

		echo $response->getBody()->getContents();

		}

	public function fullXmlReportIndividual(){

		$client = new \GuzzleHttp\Client([
			'base_uri' => 'http://192.168.1.7:3000',
			'defaults' => [
				'exceptions' => false
			]
		]);
		
		$response = $client->post('/fullXmlReportIndividual', [
			GuzzleHttp\RequestOptions::JSON => [
				'idNumber' => 56930368,
				'section' => 'PEFINDOScore',
				'subjectType' => 'Individual',
				'reportDate' => '2020-02-03'
			],
			GuzzleHttp\RequestOptions::HEADERS => ['Content-Type' => 'application/json',  'access-token' => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjQyNDgzMDQzfQ.glTwU6Lil0VPfVDL_uhR2JyZqif2_X6oVJr0Sr5xj70"]
		]);

		echo $response->getBody()->getContents();

		}

	public function fullXmlReportCompany(){

		$client = new \GuzzleHttp\Client([
			'base_uri' => 'http://192.168.1.7:3000',
			'defaults' => [
				'exceptions' => false
			]
		]);
		
		$response = $client->post('/fullXmlReportCompany', [
			GuzzleHttp\RequestOptions::JSON => [
				'idNumber' => 8408235,
				'section' => 'PEFINDOScore',
				'subjectType' => 'Company',
				'reportDate' => '2020-02-03'
			],
			GuzzleHttp\RequestOptions::HEADERS => ['Content-Type' => 'application/json',  'access-token' => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjQyNDgzMDQzfQ.glTwU6Lil0VPfVDL_uhR2JyZqif2_X6oVJr0Sr5xj70"]
		]);

		echo $response->getBody()->getContents();

		}

	public function getPdfReportIndividual(){

		$client = new \GuzzleHttp\Client([
			'base_uri' => 'http://192.168.1.7:3000',
			'defaults' => [
				'exceptions' => false
			]
		]);
		
		$response = $client->post('/getPdfReportIndividual', [
			GuzzleHttp\RequestOptions::JSON => [
				'idNumber' => 56930368,
				'reportName' => 'PEFINDOScore',
				'subjectType' => 'Individual'
			],
			GuzzleHttp\RequestOptions::HEADERS => ['Content-Type' => 'application/json',  'access-token' => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjQyNDgzMDQzfQ.glTwU6Lil0VPfVDL_uhR2JyZqif2_X6oVJr0Sr5xj70"]
		]);

		echo $response->getBody()->getContents();

		}

	public function getPdfReportCompany(){

		$client = new \GuzzleHttp\Client([
			'base_uri' => 'http://192.168.1.7:3000',
			'defaults' => [
				'exceptions' => false
			]
		]);
		
		$response = $client->post('/getPdfReportCompany', [
			GuzzleHttp\RequestOptions::JSON => [
				'idNumber' => 8408235,
				'reportName' => 'PEFINDOScore',
				'subjectType' => 'Company'
			],
			GuzzleHttp\RequestOptions::HEADERS => ['Content-Type' => 'application/json',  'access-token' => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjQyNDgzMDQzfQ.glTwU6Lil0VPfVDL_uhR2JyZqif2_X6oVJr0Sr5xj70"]
		]);

		echo $response->getBody()->getContents();

		}

	public function getCustomReportSections(){

		$client = new \GuzzleHttp\Client([
			'base_uri' => 'http://192.168.1.7:3000',
			'defaults' => [
				'exceptions' => false
			]
		]);
		
		$response = $client->post('/getCustomReportSections', [
			GuzzleHttp\RequestOptions::HEADERS => ['Content-Type' => 'application/json',  'access-token' => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjQyNDgzMDQzfQ.glTwU6Lil0VPfVDL_uhR2JyZqif2_X6oVJr0Sr5xj70"]
		]);

		echo $response->getBody()->getContents();

		}

	public function getSupportedLanguages(){

		$client = new \GuzzleHttp\Client([
			'base_uri' => 'http://192.168.1.7:3000',
			'defaults' => [
				'exceptions' => false
			]
		]);
		
		$response = $client->post('/getSupportedLanguages', [
			GuzzleHttp\RequestOptions::HEADERS => ['Content-Type' => 'application/json',  'access-token' => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjQyNDgzMDQzfQ.glTwU6Lil0VPfVDL_uhR2JyZqif2_X6oVJr0Sr5xj70"]
		]);

		echo $response->getBody()->getContents();

		}

	public function otGetReport(){

		$client = new \GuzzleHttp\Client([
			'base_uri' => 'http://192.168.1.7:3000',
			'defaults' => [
				'exceptions' => false
			]
		]);
		
		$response = $client->post('/otGetReport', [
			GuzzleHttp\RequestOptions::JSON => [
				'pefindoId' => 56930368,
				'subjectType' => 'Individual',
				'username' => '',
				'password' => ''
			],
			GuzzleHttp\RequestOptions::HEADERS => ['Content-Type' => 'application/json',  'access-token' => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjQyNDgzMDQzfQ.glTwU6Lil0VPfVDL_uhR2JyZqif2_X6oVJr0Sr5xj70"]
		]);

		echo $response->getBody()->getContents();

		}
}
